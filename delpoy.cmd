set ARCH_DIR=starter_service
set ARCH_NAME=%ARCH_DIR%_by_ruzzz.7z
set ROOT=%~dp0

del "%ROOT%\%ARCH_NAME%"
rd /s /q "%ROOT%\%ARCH_DIR%"
md "%ROOT%\%ARCH_DIR%"
copy "%ROOT%\bin\starter_service.exe" "%ROOT%\%ARCH_DIR%"
copy "%ROOT%\bin\starter_service64.exe" "%ROOT%\%ARCH_DIR%"
xcopy /e "%ROOT%\deploy" "%ROOT%\%ARCH_DIR%"
7za a -y -t7z -mx9 -mhe=on -mtc=on -m0=LZMA2 "%ROOT%\%ARCH_NAME%" "%ROOT%\%ARCH_DIR%"
rd /s /q "%ROOT%\%ARCH_DIR%"
