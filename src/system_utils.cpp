// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#include <cwchar> // wcschr
#include <string>
#include <windows.h>

#include "system_utils.hpp"

namespace rz {

std::wstring winErrorFormatW(DWORD code, DWORD lang /*= LANG_NEUTRAL*/)
{
    wchar_t *out;
    DWORD len = ::FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL,
        code, MAKELANGID(lang, SUBLANG_DEFAULT),
        reinterpret_cast<LPWSTR>(&out), 0, NULL);
    if (out && len)
    {
        wchar_t *newline = ::wcschr(out, '\r');
        if (newline)
            *newline = '\0';
        std::wstring result(out);
        ::LocalFree(out);
        return result;
    }
    else
        return std::wstring();
}

} // namespace