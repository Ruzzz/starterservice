// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#pragma once

#include "defs.hpp"

namespace rz {

// throw winapi_error
void eventLogInstallSource(const wchar_t *displayName, const wchar_t *path);
void eventLogUnInstallSource(const wchar_t *displayName);

} // namespace