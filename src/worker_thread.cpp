// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#include <sstream>
#include <windows.h>

#include "process_utils.hpp"
#include "string_utils.hpp"

#include "defs.hpp"
#include "Config.hpp"
#include "Logger.hpp"
#include "StarterService.hpp"
#include "worker_thread.hpp"

// If critical error then return false 
bool workerStep(const Config &config)
{
    static bool noSessionShow = true;
    unsigned int sid = config.sessionID();
    if (!sid)
    {
        sid = ::WTSGetActiveConsoleSessionId();
        if (sid == static_cast<DWORD>(-1) && noSessionShow)
        {
            Log::instance()->warning(L"No session attached to the physical console.");
            noSessionShow = false;
            return true;
        }
    }
    noSessionShow = true;

    try
    {
        bool sidValid;
        DWORD pid = rz::exeNamePid(config.exePath().c_str(), sid, sidValid);
        if (pid || !sidValid)
            return true;
    }
    catch (winapi_error &e)
    {
        Log::instance()->oserror(L"Cannot check target status by pid.", e.code());
        return false;
    }

    DWORD code;
    bool started;
    try
    {
        std::wstring params(config.exeParams());
        started = rz::runInSession(config.exePath().c_str(), &params[0], sid);
        code = ::GetLastError();
    }
    catch (winapi_error &e)
    {
        // ��������� ��� ��������, ����� ������������ ����������.
        // ������������ ��� �����, ��� ��� ��������� �� ����� :)
        if (!strcmp(e.what(), "WTSQueryUserToken") && e.code() == 0x000003f0)
            return true;
        else
        {
            Log::instance()->oserror(L"Cannot create process in target session.", e.code());
            return false;
        }
    }

    std::wostringstream ss;
    ss << L"\r\n"
        << L"Session ID: " << sid << "\r\n"
        << L"Path: " << config.exePath().c_str() << "\r\n"
        << L"Params: " << config.exeParams().c_str();

    if (started)
    {
        std::wstring s = L"Target started." + ss.str();
        Log::instance()->success(s.c_str());
    }
    else
    {
        // TODO: �� ����������� ���� � ���� ��������� ��������� ��� ������
        std::wstring s = L"Cannot create process in target session." + ss.str();
        Log::instance()->error(s.c_str());
    }
    return true;
}

DWORD WINAPI workerThread(PVOID context)
{
    static const size_t FIRST_PAUSE_MS = 200;
    static const size_t CHECK_INTERVAL_MS = 1000;

    StarterService::ServiceITC &itc = *static_cast<StarterService::ServiceITC*>(context);
    HANDLE workerStopped = itc.workerStopped;
    RZ_EXIT_SCOPE([workerStopped]() { ::SetEvent(workerStopped); });

    size_t trial = (60 * 60 * 1000) / CHECK_INTERVAL_MS;

    std::wstring configPath(rz::modulePath());
    rz::changeFileName(configPath, CONFIG_FILENAME);

    Config config;
    if (Config::load(configPath, config))
    {
        ::Sleep(FIRST_PAUSE_MS);
        if (workerStep(config))
        {
            DWORD prevTick = ::GetTickCount();
            while (::WaitForSingleObject(itc.stopCommand, CHECK_INTERVAL_MS) == WAIT_TIMEOUT)
            {
                DWORD nowTick = GetTickCount();
                if (nowTick - prevTick >= config.interval())
                {
                    if (!workerStep(config))
                        break;

                    prevTick = nowTick;

                    --trial;
                    if (!trial)
                    {
                        Log::instance()->warning(L"Trial version, stopping.");
                        break;
                    }
                }
            }
        }
    }

    if (::WaitForSingleObject(itc.stopCommand, 0) != WAIT_OBJECT_0)
        itc.workerAbort();
    return 0;
}
