// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#pragma once

#include <string>
#include <windows.h>

namespace rz {

// throw winapi_error
std::wstring modulePath(HMODULE hModule = NULL);

// throw winapi_error, overflow_error
std::wstring modulePath(HANDLE hProcess, HMODULE hModule = NULL);

// throw winapi_error
std::wstring processPath(DWORD pid);

// throw winapi_error
bool runInSession(const wchar_t *exePath, wchar_t *params, DWORD sessionId);

// throw winapi_error
DWORD exeNamePid(const wchar_t *path, DWORD sessionId, bool &sidValid);

} // namespace