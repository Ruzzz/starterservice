// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#pragma once

#include <string>
#include "windows.h"

class Config
{
public:
    // TODO ���� ���� ���� ���������, ���������, ������������ � ������������
    // ������ � ����� �����, ������� ����� ���.

    static bool load(const std::wstring &path, Config &out);

    static const unsigned int MIN_SECONDS = 5;
    static const unsigned int MAX_SECONDS = (60 * 60 * 24);

    const std::wstring& exePath() const { return exePath_; };
    const std::wstring& exeParams() const { return exeParams_; }
    DWORD sessionID() const { return sessionID_; };
    // � �������������, ���� � ������-����� � ��������
    unsigned int interval() const { return interval_; };

private:
    std::wstring exePath_;
    std::wstring exeParams_;
    DWORD sessionID_;
    unsigned int interval_;
};
