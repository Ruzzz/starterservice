// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#pragma once

#include <string>
#include <windows.h>

namespace rz {

class ServiceBase
{
public:
    static bool run(ServiceBase &service);

    ServiceBase(const wchar_t *name, DWORD serviceType = SERVICE_WIN32_OWN_PROCESS,
        bool canStop = true, bool canShutdown = true,
        bool canPauseResume = false);
    virtual ~ServiceBase() {}
    virtual bool valid() { return true; }
    void stop();

protected:
    void setServiceStatus(DWORD currentState, DWORD winExitCode = NO_ERROR,
        DWORD waitHint = 0);

    virtual bool onStart(DWORD argc, wchar_t **argv)
    {
        UNREFERENCED_PARAMETER(argc);
        UNREFERENCED_PARAMETER(argv);
        return true;
    }
    virtual void onStop() {}
    virtual void onPause() {}
    virtual void onResume() {}
    virtual void onShutdown() {}

private:
    friend struct ServiceDebugHelper;

    static void WINAPI serviceMain(DWORD argc, wchar_t **argv);
    static void WINAPI serviceCtrlHandler(DWORD ctrl);
    static ServiceBase *service_;

    void start(DWORD argc, wchar_t **argv);
    void pause();
    void resume();
    void shutdown();

    std::wstring name_;
    SERVICE_STATUS status_;
    SERVICE_STATUS_HANDLE statusHandle_;
    bool isDebug_;
};

struct ServiceDebugHelper
{
    static void start(ServiceBase &service, DWORD argc, wchar_t **argv);
};

} // namespace