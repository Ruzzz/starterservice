// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#pragma once

#include "ServiceBase.hpp"

class StarterService : public rz::ServiceBase
{
public:

    // 'Inter thread communication'
    class ServiceITC
    {
    public:
        ServiceITC() = delete;
        ServiceITC(StarterService &holder, HANDLE stopCommandA, HANDLE workerStoppedA)
            : stopCommand(stopCommandA),
              workerStopped(workerStoppedA),
              holder_(holder) { }
        ~ServiceITC() { }
        void workerAbort() { holder_.workerAbort_ = true;  holder_.stop(); }

        const HANDLE stopCommand;
        const HANDLE workerStopped;

    private:
        StarterService &holder_;
    };

    StarterService(const wchar_t *name, DWORD serviceType = SERVICE_WIN32_OWN_PROCESS,
        bool canStop = true, bool canShutdown = true, bool canPauseResume = false);
    ~StarterService() override;
    
    bool valid() override;
    void waitWorker();
    bool canWork(DWORD timeout = 0);

private:
    friend class ServiceITC;

    bool onStart(DWORD argc, PWSTR *argv) override;
    void onStop() override;

    ServiceITC itc_;
    bool workerAbort_;
};

inline
bool StarterService::valid()
{
    return NULL != itc_.stopCommand && NULL != itc_.workerStopped;
}

inline
void StarterService::waitWorker()
{
    ::WaitForSingleObject(itc_.workerStopped, INFINITE);
}

inline
bool StarterService::canWork(DWORD timeout /*= 0*/)
{
    return ::WaitForSingleObject(itc_.stopCommand, timeout) == WAIT_TIMEOUT;
}