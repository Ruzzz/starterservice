// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#pragma once

namespace rz {

std::wstring winErrorFormatW(DWORD code, DWORD lang = LANG_NEUTRAL);

} // namespace