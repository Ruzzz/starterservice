// Author: Ruslan Zaporojets
// Email:  ruzzzua[]gmail.com
// Date:   2016-02-18

#pragma once

// CODE MAP:
//
// #ifdef _WIN32
//   #include <windows.h>
//   #ifdef __cplusplus
//     extern "C" {
//   #endif
//     - C BASE CODE -
//   #ifdef __cplusplus
//     } // extern "C"
//   #endif
//   #ifndef __cplusplus
//     - C VERSION -
//   #else // !__cplusplus
//     - CPP VERSION -
//   #endif // !__cplusplus
// #endif // _WIN32

#ifdef _WIN32

#include <windows.h>

//
// --- Base -------------------------------------------------------------------
//

#ifdef __cplusplus
extern "C" {
#endif

// Usage:
// 1. Get len (*_len)
// 2. Allocate buffer
// 3. Convert (*_conv)

// Return needed num of wide chars
__forceinline
int winconv_toUtf16_len(const char *src,
    int srcBytes /*= -1*/,
    UINT fromCodepage /*= CP_ACP*/)
{
    if (!(src && srcBytes))
        return 0;
    if (srcBytes < -1)
        srcBytes = -1;
	int needed = MultiByteToWideChar(fromCodepage, 0, src, srcBytes, NULL, 0);
    // ���� ������� ������ ������ (srcBytes) � � ����� ��� ��� ������� \0,
    // �� ������� ������ ������ ��� ����� ������� \0,
    // ������� ����������� ����� ��� ����.
    if (!(srcBytes == -1 || src[static_cast<ptrdiff_t>(srcBytes) - 1] == L'\0')) // 1. STRING_END
        ++needed;
    return  needed;
}

// Return written num of wide chars
__forceinline
int winconv_toUtf16_conv(wchar_t *dest,
    int destChars,
    const char *src,
    int srcBytes /*= -1*/,
    UINT fromCodepage /*= CP_ACP*/)
{
	if (!(src && srcBytes && dest && destChars > 0))
		return 0;
    if (srcBytes < -1)
        srcBytes = -1;
    int written = MultiByteToWideChar(fromCodepage, 0, src, srcBytes, dest, destChars);
    // TODO If the input byte/char sequences are invalid, returns U+FFFD for UTF encodings.
    if (written > 0 && SUCCEEDED(GetLastError()))
    {
        // ���� ������� ������ ������ (srcBytes) � � ����� ��� ��� ������� \0,
        // �� ������� �� ������� ������ \0 � ���������.
        // �� ����� �� ���� �������������� ����� ��� ���� ������,
        // ������� ������ ������� ��� ����.
        if (dest[static_cast<ptrdiff_t>(written) - 1] != L'\0') // 2. STRING_END
        {
            dest[static_cast<ptrdiff_t>(written)] = L'\0';
            ++written;
        }
        return written;
    }
    return 0;
}

// Return needed num of bytes
__forceinline
int winconv_utf16To_len(const wchar_t *src,
    int srcChars /*= -1*/,
    UINT toCodepage /*= CP_ACP*/)
{
    if (!(src && srcChars))
        return 0;
    if (srcChars < -1)
        srcChars = -1;
	int needed = WideCharToMultiByte(toCodepage, 0, src, srcChars, NULL, 0, NULL, NULL);
    // ���� ������� ������ ������ (srcChars) � � ����� ��� ��� ������� \0,
    // �� ������� ������ ������ ��� ����� ������� \0,
    // ������� ����������� ����� ��� ����.
    if (!(srcChars == -1 || src[static_cast<ptrdiff_t>(srcChars) - 1] == L'\0')) // 1. STRING_END
        ++needed;
    return  needed;
}

// Return written num of bytes
__forceinline
int winconv_utf16To_conv(char *dest,
    int destBytes,
    const wchar_t *src,
    int srcChars /*= -1*/,
    UINT toCodepage /*= CP_ACP*/)
{
	if (!(src && srcChars && dest && destBytes > 0))
		return 0;
    if (srcChars < -1)
        srcChars = -1;
	int written = WideCharToMultiByte(toCodepage, 0, src, srcChars, dest, destBytes, NULL, NULL);
    // TODO If the input byte/char sequences are invalid, returns U+FFFD for UTF encodings.
    if (written > 0 && SUCCEEDED(GetLastError()))
    {
        // ���� ������� ������ ������ (srcChars) � � ����� ��� ��� ������� \0,
        // �� ������� �� ������� ������ \0 � ���������.
        // �� ����� �� ���� �������������� ����� ��� ���� ������,
        // ������� ������ ������� ��� ����.
        if (dest[static_cast<ptrdiff_t>(written) - 1] != L'\0') // 2. STRING_END
        {
            dest[static_cast<ptrdiff_t>(written)] = L'\0';
            ++written;
        }
        return written;
    }
    return 0;
}

#ifdef __cplusplus
} // extern "C"
#endif

//
// --- C Version --------------------------------------------------------------
//

#ifndef __cplusplus

// #include <stdlib.h> // malloc, free

#ifndef RZ_WIN_CONV_ALLOC
#define RZ_WIN_CONV_ALLOC(n) malloc(n)
// #define RZ_WIN_CONV_ALLOC(n) HeapAlloc(GetProcessHeap(), 0, (n))
#endif

#ifndef RZ_WIN_CONV_FREE
#define RZ_WIN_CONV_FREE(p) free(p)
// #define RZ_WIN_CONV_FREE(p) HeapFree(GetProcessHeap(), 0, (p))
#endif

inline
wchar_t* toUtf16(const char *src,
    int srcBytes /*= -1*/,
    UINT fromCodepage /*= CP_ACP*/,
	int *writtenChars /*= NULL*/)
{
	int written = 0;
    wchar_t *buf = NULL;
	int numChars = winconv_toUtf16_len(src, srcBytes, fromCodepage);
    if (numChars)
    {
        buf = (wchar_t *)RZ_WIN_CONV_ALLOC(numChars * sizeof(wchar_t));
		if (buf)
		{
			written = winconv_toUtf16_conv(buf, numChars, src, srcBytes, fromCodepage);
			if (!written)
			{
                RZ_WIN_CONV_FREE(buf);
				buf = NULL;
			}
		}
    }
	if (writtenChars)
		*writtenChars = written;
    return buf;
}

inline
char* utf16To(const wchar_t *src,
    int srcChars /*= -1*/,
    UINT toCodepage /*= CP_ACP*/,
	int *writtenBytes /*= NULL*/)
{
	int written = 0;
    char *buf = NULL;
	int numBytes = winconv_utf16To_len(src, srcChars, toCodepage);
    if (numBytes)
    {
        buf = (char *)RZ_WIN_CONV_ALLOC(numBytes);
		if (buf)
		{
			written = winconv_utf16To_conv(buf, numBytes, src, srcChars, toCodepage);
			if (!written)
			{
                RZ_WIN_CONV_FREE(buf);
				buf = NULL;
			}
		}
    }
	if (writtenBytes)
		*writtenBytes = written;
    return buf;
}

inline
char* strConv(UINT fromCodepage,
    UINT toCodepage,
    const char *src,
    int srcBytes /*= -1*/)
{
    wchar_t* tempBuf = toUtf16(src, srcBytes, fromCodepage, NULL);
    if (tempBuf)
    {
        char* result = utf16To(tempBuf, -1, toCodepage, NULL);
        RZ_WIN_CONV_FREE(tempBuf);
        return result;
    }
    return NULL;
}

__forceinline
wchar_t* utf8ToUtf16(const char *src, int srcBytes /*= -1*/)
{
    return toUtf16(src, srcBytes, CP_UTF8, NULL);
}

__forceinline
wchar_t* ansiToUtf16(const char *src, int srcBytes /*= -1*/)
{
    return toUtf16(src, srcBytes, CP_ACP, NULL);
}

__forceinline
wchar_t* oemToUtf16(const char *src, int srcBytes /*= -1*/)
{
    return toUtf16(src, srcBytes, CP_OEMCP, NULL);
}

__forceinline
char* utf16ToUtf8(const wchar_t *src, int srcChars /*= -1*/)
{
    return utf16To(src, srcChars, CP_UTF8, NULL);
}

__forceinline
char* utf16toAnsi(const wchar_t *src, int srcChars /*= -1*/)
{
    return utf16To(src, srcChars, CP_ACP, NULL);
}

__forceinline
char* utf16toOem(const wchar_t *src, int srcChars /*= -1*/)
{
    return utf16To(src, srcChars, CP_OEMCP, NULL);
}

#else // !__cplusplus

//
// --- CPP Version ------------------------------------------------------------
//

#include <string>
#include <vector>

namespace rz {
namespace str {
namespace win {

template <typename Out, typename InIt>
void toUtf16(Out &out, const InIt &from, const InIt &to, UINT fromCodepage)
{
    const char *in = static_cast<const char*>(&(*from));
    int numBytes = static_cast<int>(to - from);
    int numChars = winconv_toUtf16_len(in, numBytes, fromCodepage);
    if (numChars)
    {
        out.resize(numChars);
        int written = winconv_toUtf16_conv(&out[0], numChars, in, numBytes,
            fromCodepage);
        if (!written)
            out.clear();
        // else
        //     out.resize(written);
    }
}

template <typename Out, typename InIt>
void utf16To(Out &out, const InIt &from, const InIt &to, UINT toCodepage)
{
    const wchar_t *in = static_cast<const wchar_t*>(&(*from));
    int numChars = static_cast<int>(to - from);
    int numBytes = winconv_utf16To_len(in, numChars, toCodepage);
    if (numBytes)
    {
        out.resize(numBytes);
        int written = winconv_utf16To_conv(&out[0], numBytes, in, numChars,
            toCodepage);
        if (!written)
            out.clear();
        // else
        //     out.resize(written);
    }
}

} // namespace win
} // namespace str
} // namespace rz

#endif // !__cplusplus

#endif // _WIN32