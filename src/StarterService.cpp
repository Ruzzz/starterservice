// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#include <string>
#include <windows.h>

#include "defs.hpp"
#include "Logger.hpp"
#include "worker_thread.hpp"
#include "StarterService.hpp"

StarterService::StarterService(const wchar_t *name, DWORD serviceType,
    bool canStop, bool canShutdown, bool canPauseResume)
    : rz::ServiceBase(name, serviceType, canStop, canShutdown, canPauseResume),
      itc_(*this, ::CreateEvent(NULL, TRUE, FALSE, NULL),
          ::CreateEvent(NULL, TRUE, FALSE, NULL) ),
      workerAbort_(false)
{
}

StarterService::~StarterService()
{
    if (itc_.stopCommand)
        ::CloseHandle(itc_.stopCommand);
    if (itc_.workerStopped)
        ::CloseHandle(itc_.workerStopped);
}

bool StarterService::onStart(DWORD argc, PWSTR *argv)
{
    UNREFERENCED_PARAMETER(argc);
    UNREFERENCED_PARAMETER(argv);    
    if (::QueueUserWorkItem(&workerThread, &itc_, WT_EXECUTELONGFUNCTION) == FALSE)
        return false;
    Log::instance()->info(L"Service started.");
    return true;
}

void StarterService::onStop()
{
    if (!workerAbort_)
    {
        ::SetEvent(itc_.stopCommand);
        waitWorker();
    }
    Log::instance()->info(L"Service stopped.");
}
