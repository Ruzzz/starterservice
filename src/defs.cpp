// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-12-31

#include "defs.hpp"

const char *SERVICE_VERSION_A  = "v1.3 (2016-03-15)";
const char *EXE_NAME_A         = "starter_service.exe";
const wchar_t *CONFIG_FILENAME = L"starter_service.ini";
const char *CONFIG_FILENAME_A  = "starter_service.ini";

const wchar_t *SERVICE_NAME         = L"StarterService";
const wchar_t *SERVICE_DISPLAY_NAME = L"Starter Service";
const char *SERVICE_DISPLAY_NAME_A  = "Starter Service";
const wchar_t *SERVICE_DEPENDENCIES = L"";
