// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#pragma once

#include <mutex> // TODO Logger.cpp
#include "windows.h"

class ILogger
{
public:
    enum ErrorType { SUCCESS, INFORMATION, WARNING, LOGIC_ERROR };

    ILogger() {}
    virtual ~ILogger() {}

    void success(const wchar_t *s) const { log(s, SUCCESS); };
    void info(const wchar_t *s) const    { log(s, INFORMATION); };
    void warning(const wchar_t *s) const { log(s, WARNING); };
    void error(const wchar_t *s) const   { log(s, LOGIC_ERROR); };

    void success(const std::wstring &s) const { log(s.c_str(), SUCCESS); };
    void info(const std::wstring &s) const    { log(s.c_str(), INFORMATION); };
    void warning(const std::wstring &s) const { log(s.c_str(), WARNING); };
    void error(const std::wstring &s) const   { log(s.c_str(), LOGIC_ERROR); };

    void oserror(const wchar_t *s, DWORD code) const;

    void oserror(const std::wstring &s, DWORD code) const 
    {
        oserror(s.c_str(), code);
    }

private:
    virtual void log(const wchar_t *message, ErrorType errorType) const = 0;
};

class Log
{
public:
    static ILogger* instance(bool console = false);

    Log() = delete;
    Log(Log&) = delete;
    void operator =(Log&) = delete;
};
