// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#include <string>
#include <windows.h>

#include "defs.hpp"
#include "eventlog_utils.hpp"

namespace rz {

void eventLogInstallSource(const wchar_t *displayName, const wchar_t *path)
{
    std::wstring rpath(
        L"SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application\\");
    rpath.append(displayName);

    HKEY key;
    LSTATUS code = ::RegCreateKeyExW(HKEY_LOCAL_MACHINE, rpath.c_str(), 0, 0,
        REG_OPTION_NON_VOLATILE, KEY_SET_VALUE, 0, &key, 0);
    if (code != ERROR_SUCCESS)
        throw winapi_error("RegCreateKeyExW", code);
    RZ_EXIT_SCOPE([key]() { ::RegCloseKey(key); });

    code = ::RegSetValueExW(key, L"EventMessageFile", 0, REG_SZ,
        reinterpret_cast<const BYTE *>(path),
        static_cast<DWORD>(wcslen(path) * sizeof(wchar_t)));
    if (code != ERROR_SUCCESS)
        throw winapi_error("RegSetValueEx", code);

    const DWORD types = EVENTLOG_SUCCESS | EVENTLOG_INFORMATION_TYPE |
        EVENTLOG_WARNING_TYPE | EVENTLOG_ERROR_TYPE;
    code = ::RegSetValueExW(key, L"TypesSupported", 0, REG_DWORD,
        reinterpret_cast<const BYTE *>(&types), static_cast<DWORD>(sizeof(types)));
    if (code != ERROR_SUCCESS)
        throw winapi_error("RegSetValueEx", code);
}

void eventLogUnInstallSource(const wchar_t *displayName)
{
    std::wstring rpath(
        L"SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application\\");
    rpath.append(displayName);

    LSTATUS code = ::RegDeleteKeyW(HKEY_LOCAL_MACHINE, rpath.c_str());
    if (code != ERROR_SUCCESS)
        throw winapi_error("RegDeleteKeyW", code);
}

} // namespace