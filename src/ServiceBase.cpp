// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#include "ServiceBase.hpp"

namespace rz {

ServiceBase *ServiceBase::service_ = nullptr;

bool ServiceBase::run(ServiceBase &service)
{
    service_ = &service;
    SERVICE_TABLE_ENTRYW serviceTable[] =
    {
        { &service.name_[0], serviceMain },
        { NULL, NULL }
    };
    // Note: If FALSE && GetLastError() == ERROR_FAILED_SERVICE_CONTROLLER_CONNECT
    //       then running not as service.
    return ::StartServiceCtrlDispatcherW(serviceTable) != FALSE;
}

void WINAPI ServiceBase::serviceMain(DWORD argc, wchar_t **argv)
{
    service_->statusHandle_ =
        ::RegisterServiceCtrlHandlerW(service_->name_.c_str(), serviceCtrlHandler);
    if (!service_->statusHandle_)
        // RegisterServiceCtrlHandlerW return NULL
        ::OutputDebugStringA("com.ruzzz.ServiceBase.< Educa te ipsum! >");
    else
        service_->start(argc, argv);
}

void WINAPI ServiceBase::serviceCtrlHandler(DWORD ctrl)
{
    switch (ctrl)
    {
        case SERVICE_CONTROL_STOP:
            service_->stop();
            break;
        case SERVICE_CONTROL_PAUSE:
            service_->pause();
            break;
        case SERVICE_CONTROL_CONTINUE:
            service_->resume();
            break;
        case SERVICE_CONTROL_SHUTDOWN:
            service_->shutdown();
            break;
        case SERVICE_CONTROL_INTERROGATE:
            break;
        default:
            break;
    }
}

// Ctor

ServiceBase::ServiceBase(const wchar_t *name, DWORD serviceType,
    bool canStop, bool canShutdown, bool canPauseResume)
    : name_(name),
      status_({ 0 }),
      statusHandle_(NULL),
      isDebug_(false)
{
    status_.dwServiceType = serviceType;
    status_.dwCurrentState = SERVICE_START_PENDING;
    DWORD controlsAccepted = 0;
    if (canStop)
        controlsAccepted |= SERVICE_ACCEPT_STOP;
    if (canShutdown)
        controlsAccepted |= SERVICE_ACCEPT_SHUTDOWN;
    if (canPauseResume)
        controlsAccepted |= SERVICE_ACCEPT_PAUSE_CONTINUE;
    status_.dwControlsAccepted = controlsAccepted;
    status_.dwWin32ExitCode = NO_ERROR;
    status_.dwServiceSpecificExitCode = 0;
    status_.dwCheckPoint = 0;
    status_.dwWaitHint = 0;
}

// Protected helpers

void ServiceBase::setServiceStatus(DWORD currentState, DWORD winExitCode,
    DWORD waitHint)
{
    static DWORD checkPoint = 1;

    status_.dwCurrentState = currentState;
    status_.dwWin32ExitCode = winExitCode;
    status_.dwWaitHint = waitHint;
    status_.dwCheckPoint = ((currentState == SERVICE_RUNNING) ||
        (currentState == SERVICE_STOPPED)) ? 0 : checkPoint++;
    if (!isDebug_)
        ::SetServiceStatus(statusHandle_, &status_);
}

// Private methods

void ServiceBase::start(DWORD argc, wchar_t **argv)
{
    bool ok = false;
    try
    {
        setServiceStatus(SERVICE_START_PENDING);
        ok = onStart(argc, argv);
    }
    catch (...)
    {
        ok = false;
    }
    setServiceStatus(ok ? SERVICE_RUNNING : SERVICE_STOPPED);
}

void ServiceBase::stop()
{
    DWORD originalState = status_.dwCurrentState;
    try
    {
        setServiceStatus(SERVICE_STOP_PENDING);
        onStop();
        setServiceStatus(SERVICE_STOPPED);
    }
    catch (...)
    {
        setServiceStatus(originalState);
    }
}

void ServiceBase::pause()
{
    try
    {
        setServiceStatus(SERVICE_PAUSE_PENDING);
        onPause();
        setServiceStatus(SERVICE_PAUSED);
    }
    catch (...)
    {
        setServiceStatus(SERVICE_RUNNING);
    }
}

void ServiceBase::resume()
{
    try
    {
        setServiceStatus(SERVICE_CONTINUE_PENDING);
        onResume();
        setServiceStatus(SERVICE_RUNNING);
    }
    catch (...)
    {
        setServiceStatus(SERVICE_PAUSED);
    }
}

void ServiceBase::shutdown()
{
    __try
    {
        onShutdown();
    }
    __finally
    {
        setServiceStatus(SERVICE_STOPPED);
    }    
}

void ServiceDebugHelper::start(ServiceBase &service, DWORD argc, wchar_t **argv)
{
    service.isDebug_ = true;
    service.start(argc, argv);
}

} // namespace