// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#include <vector>
#include <algorithm> // std::min

#include <windows.h>
#include <Psapi.h>                // EnumProcesses, GetModuleFileNameExW
#pragma comment(lib, "Psapi.lib") // GetModuleFileNameEx, or Kernel32.lib - see doc
#include <WtsApi32.h>             // WTSQueryUserToken
#pragma comment(lib, "WtsApi32.lib")
#include <Userenv.h>              // CreateEnvironmentBlock 
#pragma comment(lib, "Userenv.lib")

#include "defs.hpp"
#include "process_utils.hpp"

namespace rz {

// GetModuleFileName
// -----------------
//
// ���������� GetLastError() == ERROR_INSUFFICIENT_BUFFER ���� WIN > WinXP
//
// 1. ������ COPIED_CHARS_COUNT ��� /0
//    - ���� ����� ����������� ����� (� ��� ����� ��� \0).
//
// 2. ������ COPIED_CHARS_COUNT  +  /0
//    - ���� ��� ������ (������ � \0) �� ����������.
// 
// ���������� ��� GetModuleFileNameEx() == BUFFER_LEN
// ����� ���� ������ �� ������ ������, �.�. ����� ������ �� �������.

// ����� ������������ ERROR_INSUFFICIENT_BUFFER
// ��� ��� WinXP �������� ���� �� � ����� \0:
//
//   if ((out.size() == written && out[written - 1] != L'\0') // For WinXP
//     || ::GetLastError() == ERROR_INSUFFICIENT_BUFFER)
//
// �� ���������� ��� ���������� WRITTEN < BUFFER_LEN.
// ��������� modulePath ��� ����.

// throw winapi_error
std::wstring modulePath(HMODULE hModule /*= NULL*/)
{
    const size_t NTFS_MAX_PATH = 32768U - 1;
    std::wstring out;
    out.resize(MAX_PATH);

loop:
    DWORD written = ::GetModuleFileNameW(hModule, &out[0],
        static_cast<DWORD>(out.size()));
    if (!written)
        throw winapi_error("GetModuleFileNameExW", ::GetLastError());
    else if (static_cast<size_t>(written) < out.size())
        return std::wstring(out.c_str()); // TODO
    else if (out.size() == NTFS_MAX_PATH)
        throw std::overflow_error("GetModuleFileNameExW");
    else
        out.resize((std::min)(out.size() * 2, NTFS_MAX_PATH));
    goto loop;
}

// GetModuleFileNameEx
// -------------------
//
// �� ���������� GetLastError() == ERROR_INSUFFICIENT_BUFFER.
//
// 1. ������ COPIED_CHARS_COUNT ��� /0
//    - ���� ����� ����������� ����� (� ��� ����� ��� \0).
//
// 2. ������ COPIED_CHARS_COUNT  +  /0
//    - ���� ��� ������ (������ � \0) �� ����������.
//
// ���������� ��� GetModuleFileNameExW() == BUFFER_LEN
// ����� ���� ������ �� ������ ������, �.�. ����� ������ �� �������.

// throw winapi_error
// throw overflow_error
std::wstring modulePath(HANDLE hProcess, HMODULE hModule /*= NULL*/)
{
    const size_t NTFS_MAX_PATH = 32768U - 1;
    std::wstring out;
    out.resize(MAX_PATH);

loop:
    DWORD written = ::GetModuleFileNameExW(hProcess, hModule, &out[0],
        static_cast<DWORD>(out.size()));
    if (!written)
        throw winapi_error("GetModuleFileNameExW", ::GetLastError());
    else if (static_cast<size_t>(written) < out.size())
        return out;
    else if (out.size() == NTFS_MAX_PATH)
        throw std::overflow_error("GetModuleFileNameExW");
    else
        out.resize((std::min)(out.size() * 2, NTFS_MAX_PATH));
    goto loop;
}

// throw winapi_error
std::wstring processPath(DWORD pid)
{
    HANDLE hProcess = ::OpenProcess(PROCESS_QUERY_INFORMATION |
        PROCESS_VM_READ, FALSE, pid);
    if (!hProcess)
        throw winapi_error("OpenProcess", ::GetLastError());
    RZ_EXIT_SCOPE([hProcess]() { ::CloseHandle(hProcess); });
    return modulePath(hProcess);
}

// ����� �������� ������ GetLastError()
// ��������� ������ CreateProcessAsUserW - ��� ���� �� "������ ������ ������",
//   ������� �� ������ throw, �� ��� ������������� �������� 
//   ������������ ������.

// throw winapi_error
bool runInSession(const wchar_t *exePath, wchar_t *params, DWORD sessionId)
{
    HANDLE hUserToken = 0;

    // Only for SYSTEM
    if (!::WTSQueryUserToken(sessionId, &hUserToken))
        throw winapi_error("WTSQueryUserToken", ::GetLastError());

    STARTUPINFOW si = { 0 };
    si.cb = sizeof(STARTUPINFO);
    si.lpDesktop = L"winsta0\\default";
    PROCESS_INFORMATION pi = { 0 };
    // DWORD flags = show ? 0 : CREATE_NO_WINDOW;  // TODO
    DWORD flags = 0;
    void *pEnv;
    if (::CreateEnvironmentBlock(&pEnv, hUserToken, TRUE))
        flags |= CREATE_UNICODE_ENVIRONMENT;
    else
        pEnv = NULL;

    bool ret = ::CreateProcessAsUserW(hUserToken, exePath, params, NULL, NULL,
        FALSE, flags, pEnv, NULL, &si, &pi) != FALSE;
    ::CloseHandle(pi.hThread);
    ::CloseHandle(pi.hProcess);
    ::CloseHandle(hUserToken);
    return ret;
}

// throw winapi_error
// TODO Check sessionId is valid?
DWORD exeNamePid(const wchar_t *path, DWORD sessionId, bool &sidValid)
{
    if (!path)
        return 0;
    std::vector<DWORD> buf(512);

loop:
    DWORD written;
    // Doc: Therefore, if written == cb, consider retrying the call with a larger array.
    BOOL ok = ::EnumProcesses(&buf[0], static_cast<DWORD>(buf.size()), &written);
    if (!ok)
        throw winapi_error("EnumProcesses", ::GetLastError());
    if (buf.size() == static_cast<size_t>(written))
    {
        buf.resize(buf.size() * 2); // TODO overflow
        goto loop;
    }

    sidValid = false;
    for (DWORD pid : buf)
    {
        if (pid == 0)
            continue;
        DWORD psid;
        if (!::ProcessIdToSessionId(pid, &psid))
            throw winapi_error("ProcessIdToSessionId", ::GetLastError());
        if (sessionId != psid)
            continue;
        sidValid = true;

        std::wstring ppath;
        try
        {
            ppath = rz::processPath(pid);
        }
        catch (winapi_error &e)
        {
            // ERROR_ACCESS_DENIED  - OpenProcess
            // ERROR_INVALID_HANDLE - Handle invalid, GetModuleFileNameExW
            // 0x12B -              - ?? OpenProcess, �������� ������� ��� �����������?
            if ((e.code() == ERROR_ACCESS_DENIED  && !strcmp(e.what(), "OpenProcess")) ||
                (e.code() == ERROR_INVALID_HANDLE && !strcmp(e.what(), "GetModuleFileNameExW")) ||
                (e.code() == 0x12Bu && !strcmp(e.what(), "OpenProcess")))
                continue;
            else
                return 0;

        }
        if (!lstrcmpiW(path, ppath.c_str()))
            return pid;
    }
    return 0;
}

} // namespace