// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#include "vector"

#include "rz_str_winconv.h"
#include "string_utils.hpp"
#include "file_utils.hpp"

namespace rz {

bool fileRead(const wchar_t *filename, void *buf, DWORD size)
{
	bool ok = false;
	if (buf && size)
	{
		HANDLE hfile = ::CreateFileW(filename, GENERIC_READ, FILE_SHARE_READ, NULL,
			OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
		if (INVALID_HANDLE_VALUE != hfile)
		{
			DWORD written;
			ok = !!(::ReadFile(hfile, buf, size, &written, NULL) && (written == size));
            ::CloseHandle(hfile);
		}
	}
	return ok;
}

bool fileReadAsUtf8(const std::wstring  &filename, std::wstring &out)
{
    DWORD size = fileSize(filename.c_str());
    if (!size)
        return false;
    std::vector<char> buf(size);
    if (!fileRead(filename.c_str(), &buf[0], size))
        return false;

    auto bufIt = buf.begin();
    utf8SkipBom(bufIt, buf.end());
    rz::str::win::toUtf16(out, bufIt, buf.end(), CP_UTF8);

    return true;
}

} // namespace