// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#include "service_utils.hpp"

namespace rz {

// --- Return WinAPI Errors ---------------------------------------------------

#define WIN_SERVICE_DEFAULT_WAIT 200

BOOL serviceInstall(const wchar_t *name, const wchar_t *displayName,
    const wchar_t *path, DWORD startType, DWORD serviceType,
    const wchar_t *dependencies, const wchar_t *account, const wchar_t *password)
{
    SC_HANDLE hSCManager = ::OpenSCManagerW(NULL, NULL, SC_MANAGER_CONNECT |
        SC_MANAGER_CREATE_SERVICE);
    if (!hSCManager)
        return FALSE;
    else
    {
        SC_HANDLE hService = ::CreateServiceW(hSCManager, name, displayName,
            SERVICE_QUERY_STATUS, serviceType, startType, SERVICE_ERROR_NORMAL,
            path, NULL, NULL, dependencies, account, password);

        DWORD code;
        if (hService)
        {
            code = ERROR_SUCCESS;
            ::CloseServiceHandle(hService);
        }
        else
            code = GetLastError();

        ::CloseServiceHandle(hSCManager);
        ::SetLastError(code);
        return code == ERROR_SUCCESS;
    } // if (!hSCManager) else
}

BOOL serviceUninstall(const wchar_t *name)
{
    SC_HANDLE hSCManager = ::OpenSCManagerW(NULL, NULL, SC_MANAGER_CONNECT);
    if (!hSCManager)
        return FALSE;
    else
    {
        SC_HANDLE hService = ::OpenServiceW(hSCManager, name, SERVICE_STOP |
            SERVICE_QUERY_STATUS | DELETE);
        
        DWORD code;
        if (!hService)
            code = ::GetLastError();
        else
        {
            SERVICE_STATUS status;
            RtlZeroMemory(&status, sizeof(status));
            if (ControlService(hService, SERVICE_CONTROL_STOP, &status))
            {
                ::Sleep(status.dwWaitHint ? status.dwWaitHint : WIN_SERVICE_DEFAULT_WAIT);
                while (::QueryServiceStatus(hService, &status))
                {
                    if (status.dwCurrentState == SERVICE_STOP_PENDING)
                        ::Sleep(status.dwWaitHint ? status.dwWaitHint : WIN_SERVICE_DEFAULT_WAIT);
                    else
                        break;
                }
                // TODO if (status.dwCurrentState != SERVICE_STOPPED)
            }

            // ���� ���� ������� � ������ ControlService ���� ������,
            // ��� ����� �������� ������� ������
            if (!::DeleteService(hService))
                code = ::GetLastError();
            else
                code = ERROR_SUCCESS; // TODO See MSDN DeleteService )
            ::CloseServiceHandle(hService);
        } // if (!hService) else

        ::CloseServiceHandle(hSCManager);
        ::SetLastError(code);
        return code == ERROR_SUCCESS;
    } // if (!hSCManager) else
}

BOOL serviceStart(const wchar_t *name)
{
    SC_HANDLE hSCManager = ::OpenSCManagerW(NULL, NULL, SC_MANAGER_CONNECT);
    if (!hSCManager)
        return FALSE;
    else
    {
        DWORD code;
        SC_HANDLE hService = ::OpenServiceW(hSCManager, name, SERVICE_START);
        if (!hService)
            code = ::GetLastError();
        else
        {
            if (!::StartServiceW(hService, 0, NULL))
                code = ::GetLastError();
            else
                code = ERROR_SUCCESS; // TODO See MSDN StartService )
            ::CloseServiceHandle(hService);
        }

        ::CloseServiceHandle(hSCManager);
        ::SetLastError(code);
        return code == ERROR_SUCCESS;
    }
}

BOOL serviceStop(const wchar_t *name)
{
    // TODO: StopDependentServices()
    // Ex. see https://msdn.microsoft.com/en-us/library/ms686335(v=vs.85).aspx

    SC_HANDLE hSCManager = ::OpenSCManagerW(NULL, NULL, SC_MANAGER_CONNECT);
    if (!hSCManager)
        return FALSE;
    else
    {
        SC_HANDLE hService = ::OpenServiceW(hSCManager, name, SERVICE_STOP |
            SERVICE_QUERY_STATUS);

        DWORD code;
        if (!hService)
            code = ::GetLastError();
        else
        {
            SERVICE_STATUS status;
            RtlZeroMemory(&status, sizeof(status));
            if (::ControlService(hService, SERVICE_CONTROL_STOP, &status))
            {
                ::Sleep(status.dwWaitHint ? status.dwWaitHint :
                    WIN_SERVICE_DEFAULT_WAIT);
                while (::QueryServiceStatus(hService, &status))
                {
                    if (status.dwCurrentState == SERVICE_STOP_PENDING)
                        ::Sleep(status.dwWaitHint ? status.dwWaitHint :
                            WIN_SERVICE_DEFAULT_WAIT);
                    else
                        break;
                }
            }

            if (status.dwCurrentState != SERVICE_STOPPED)
                code = ::GetLastError();
            else
                code = ERROR_SUCCESS;
            ::CloseServiceHandle(hService);
        } // if (!hService) else

        ::CloseServiceHandle(hSCManager);
        ::SetLastError(code);
        return code == ERROR_SUCCESS;
    } // if (!hSCManager) else
}

} // namespace