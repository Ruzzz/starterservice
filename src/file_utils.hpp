// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#pragma once

#include <string>
#include "windows.h"

namespace rz {

__forceinline
bool fileExists(const wchar_t *fileName)
{
    return ::GetFileAttributesW(fileName) != INVALID_FILE_ATTRIBUTES;
}

__forceinline
bool dirExists(const wchar_t *path)
{
    DWORD attr = ::GetFileAttributesW(path);
    return (attr != INVALID_FILE_ATTRIBUTES && (attr & FILE_ATTRIBUTE_DIRECTORY));
}

__forceinline
DWORD fileSize(const wchar_t *filename)
{
    WIN32_FILE_ATTRIBUTE_DATA fad;
    if (::GetFileAttributesExW(filename, GetFileExInfoStandard, &fad))
        return fad.nFileSizeLow;
    else
        return 0;
}

bool fileRead(const wchar_t *filename, void *buf, DWORD size);
bool fileReadAsUtf8(const std::wstring  &filename, std::wstring &out);

} // namespace