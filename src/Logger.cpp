// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <windows.h>

#include "defs.hpp"
#include "rz_str_winconv.h"
#include "event_defs.h"
#include "system_utils.hpp"

#include "Logger.hpp"

//
//  Helpers
//

namespace {

    inline
    std::string toOEM(const std::wstring &s)
    {
        std::string out;
        rz::str::win::utf16To(out, s.begin(), s.end(), CP_OEMCP);
        return out;
    }

}

//
// ILogger
//

void ILogger::oserror(const wchar_t *message, DWORD code) const
{
     std::wostringstream ss;
     ss << message
         << L"\r\n"
         << L"System error: 0x"
             << std::hex << std::setw(8) << std::setfill(L'0') << code
         << L" - " << rz::winErrorFormatW(code).c_str();
     error(ss.str().c_str());
}

//
// EventLogger
//

class EventLogger : public ILogger
{
public:
    EventLogger() = delete;
    EventLogger(const wchar_t *title)
        : ILogger(), hEventSource_(::RegisterEventSourceW(NULL, title))
    {
    }

    ~EventLogger() override
    {
        ::DeregisterEventSource(hEventSource_);
    }

    void log(const wchar_t *message, ErrorType errorType) const override;

protected:
    HANDLE hEventSource_;
};

void EventLogger::log(const wchar_t *message, ErrorType errorType) const
{
    static std::mutex m;
    std::lock_guard<std::mutex> lk(m);

    if (hEventSource_)
    {
        DWORD eventID;
        WORD eventType;
        switch (errorType)
        {
            case ErrorType::SUCCESS:
                eventType = EVENTLOG_SUCCESS;
                eventID = MSG_SUCCESS_1;
                break;
            case ErrorType::WARNING:
                eventType = EVENTLOG_WARNING_TYPE;
                eventID = MSG_WARNING_1;
                break;
            case ErrorType::LOGIC_ERROR:
                eventType = EVENTLOG_ERROR_TYPE;
                eventID = MSG_ERROR_1;
                break;
            case ErrorType::INFORMATION:
            default:
                eventType = EVENTLOG_INFORMATION_TYPE;
                eventID = MSG_INFO_1;
                break;
        }

        LPCWSTR ss[2] = { message, NULL };
        ::ReportEventW(hEventSource_, eventType, 0, eventID,
            NULL, 1, 0, ss, NULL);
    }
}

//
// ConsoleLogger
//

class ConsoleLogger : public ILogger
{
public:
    ConsoleLogger() : ILogger() {}
    ~ConsoleLogger() override {}
    void log(const wchar_t *message, ErrorType errorType) const override;
};

void ConsoleLogger::log(const wchar_t *message, ErrorType errorType) const
{
    static std::mutex m;
    std::lock_guard<std::mutex> lk(m);

    std::wstring s;
    switch (errorType)
    {
        case ILogger::SUCCESS:     s += L"Success: ";     break;
        case ILogger::INFORMATION: s += L"Information: "; break;
        case ILogger::WARNING:     s += L"Warning: ";     break;
        case ILogger::LOGIC_ERROR: s += L"Error: ";       break;
    }
    s += message;    
    std::cout << toOEM(s).c_str() << std::endl;
}

class SplitLogger : public ILogger
{
public:
    SplitLogger(const wchar_t *title) :
        ILogger(), winevent_(title), console_() {}
    ~SplitLogger() override {}
    void log(const wchar_t *message, ErrorType errorType) const override;

    EventLogger* loggerEvent() { return &winevent_; };
    ConsoleLogger* loggerConsole() { return &console_; };

private:
    EventLogger winevent_;
    ConsoleLogger console_;
};

void SplitLogger::log(const wchar_t *message, ErrorType errorType) const
{
    winevent_.log(message, errorType);
    console_.log(message, errorType);
}

//
// Logger
//

static SplitLogger logger_(SERVICE_DISPLAY_NAME);

ILogger*  Log::instance(bool console /*= false*/)
{
    // TODO Создавать не сразу
    if (console)
        return &logger_;
    else        
        return logger_.loggerEvent();
}
