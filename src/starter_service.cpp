// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

// TODO: HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Windows\NoInteractiveServices=0
// https://msdn.microsoft.com/en-us/library/windows/desktop/ms683502(v=vs.85).aspx

#if defined(_WIN32) && defined(_DEBUG)
#define _CRTDBG_MAPALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif
#include <iostream>
#include <windows.h>

#include "defs.hpp"
#include "eventlog_utils.hpp"
#include "process_utils.hpp"
#include "service_utils.hpp"
#include "Logger.hpp"
#include "StarterService.hpp"

//
//  Consts
//

const char USAGE[] =
{
    "%s %s \n"
    "Author: Ruslan Zaporojets [ruzzzua@gmail.com]\n"
    "Usage: %s command"
    "\n"
    "Commands:\n"
    "   -i or install       Install the service.\n"
    "   -s or start         Start the service.\n"
    "   -t or stop          Stop the service.\n"
    "   -u or uninstall     Uninstall the service.\n"
    "\n"
    "Set values of utf-8 config file %s:\n"
    "interval   - In seconds.\n"
    "exe_path   - Path of target exe file. Can use env vars, ex. %%SystemRoot%%.\n"
    "exe_params - [optional] Command line parameters.\n"
    // "show       - [optional] 1 (default) or 0, show or hide target app.\n"
    "session_id - [optional] ID or 0 (default) for running target as active\n"
    "             local user.\n"
};

void installService(const wchar_t *path)
{
    if (!rz::serviceInstall(SERVICE_NAME,
        SERVICE_DISPLAY_NAME, path, SERVICE_START_TYPE,
        SERVICE_TYPE, SERVICE_DEPENDENCIES, SERVICE_ACCOUNT,
        SERVICE_PASSWORD))
    {
        throw winapi_error("rz::serviceInstall", ::GetLastError());
    }
}

void uninstallService()
{
    if (!rz::serviceUninstall(SERVICE_NAME))
        throw winapi_error("rz::serviceInstall", ::GetLastError());
}

//
//  Main
//

int wmain(int argc, wchar_t *argv[])
{
    if (argc > 1)
    {
        if (::lstrcmpiW(L"-i", argv[1]) == 0 ||
            ::lstrcmpiW(L"install", argv[1]) == 0)
        {
            try
            {
                std::wstring path(rz::modulePath()); // TODO Normalize path
                installService(path.c_str());
                rz::eventLogInstallSource(SERVICE_DISPLAY_NAME, path.c_str());
            }
            catch (winapi_error &e)
            {
                Log::instance(true)->oserror(L"Install failed.", e.code());
                return -1;
            }
            std::cout << "Service installed." << std::endl;
        }

        else if (::lstrcmpiW(L"-u", argv[1]) == 0 ||
            ::lstrcmpiW(L"uninstall", argv[1]) == 0)
        {
            // Force uninstall
            bool ok = true;
            try
            {
                try
                {
                    uninstallService();
                }
                catch (winapi_error &e)
                {
                    Log::instance(true)->oserror(L"Uninstall error.", e.code());
                    ok = false;
                }
                rz::eventLogUnInstallSource(SERVICE_DISPLAY_NAME);
            }
            catch (winapi_error &e)
            {
                Log::instance(true)->oserror(L"Uninstall error.", e.code());
                ok = false;
            }
            if (!ok)
                return -1;
            std::cout << "Service uninstalled." << std::endl;
        }

        else if (::lstrcmpiW(L"-s", argv[1]) == 0 ||
            ::lstrcmpiW(L"start", argv[1]) == 0)
        {
            if (!rz::serviceStart(SERVICE_NAME))
            {
                Log::instance(true)->oserror(L"Start failed.", ::GetLastError());
                return -1;
            }
            std::cout << "Service started." << std::endl;
        }

        else if (::lstrcmpiW(L"-t", argv[1]) == 0 ||
            ::lstrcmpiW(L"stop", argv[1]) == 0)
        {
            if (!rz::serviceStop(SERVICE_NAME))
            {
                Log::instance(true)->oserror(L"Stop error.", ::GetLastError());
                return -1;
            }
            std::cout << "Service stoped." << std::endl;
        }

        else if (::lstrcmpiW(L"console", argv[1]) == 0)
        {
            StarterService service(SERVICE_NAME, SERVICE_TYPE);
            if (!service.valid())
            {
                Log::instance(true)->error(L"Fortes fortuna adjuvat.");
                return -1;
            }
            else
            {
                rz::ServiceDebugHelper::start(service, argc, argv);
                service.waitWorker();
            }
        }

        else
            printf_s(USAGE,
                SERVICE_DISPLAY_NAME_A,
                SERVICE_VERSION_A,
                EXE_NAME_A,
                CONFIG_FILENAME_A);
    }
    else
    {
        printf_s(USAGE,
            SERVICE_DISPLAY_NAME_A,
            SERVICE_VERSION_A,
            EXE_NAME_A,
            CONFIG_FILENAME_A);

        StarterService service(SERVICE_NAME, SERVICE_TYPE);
        if (!service.valid())
        {
            Log::instance(true)->error(L"Fortes fortuna adjuvat.");
            return -1;
        }
        if (!rz::ServiceBase::run(service))
        {
            DWORD code = ::GetLastError();
            // Running as interactive, ex. in console
            if (code == ERROR_FAILED_SERVICE_CONTROLLER_CONNECT)
                return 0;
            Log::instance(true)->oserror(L"StarterService run fails.", code);
            return -1;
        }
    }

#if defined(_WIN32) && defined(_DEBUG)
    _CrtDumpMemoryLeaks();
#endif
    return 0;
}