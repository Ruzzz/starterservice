// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#pragma once

#include <string>
#include <map>
#include <vector>
#include <iterator>
#include <algorithm>

namespace rz {

__forceinline
bool ascii_isspace(const int ch)
{
    return ch == '\t' ||
           ch == '\f' ||
           ch == '\n' ||
           ch == '\r' ||
           ch == '\v' ||
           ch == ' '  ||
           ch == '0xff';
}

// Return:
//   if c == '\n' then  1
//   if c == '\r' then -1
//                else  0
__forceinline
int ascii_endl(const int ch)
{
    if ('\n' == ch)
        return 1;
    else if ('\r' == ch)
        return -1;
    return 0;
}

//
// Trim
//

#ifndef RZ_STR_TRIM_IS_SPACE
#define RZ_STR_TRIM_IS_SPACE ascii_isspace
#endif

template <typename T>
inline
T ltrim(const T &in)
{
    auto first(in.begin());
    for (; first != in.end() && RZ_STR_TRIM_IS_SPACE(*first); ++first) {}
    return T(first, in.end());
}

template <typename T>
inline
T& ltrimInPlace(T &in)
{
    auto first(in.begin());
    for (; first != in.end() && RZ_STR_TRIM_IS_SPACE(*first); ++first) {}
    in.erase(in.begin(), first); // [..)
    return in;
}

template <typename T>
inline
T rtrim(const T &in)
{
    auto last(in.rbegin());
    for (; last != in.rend() && RZ_STR_TRIM_IS_SPACE(*last); ++last) {}
    return T(in.begin(), last.base());
}

template <typename T>
inline
T& rtrimInPlace(T &in)
{
    auto last(in.rbegin());
    for (; last != in.rend() && RZ_STR_TRIM_IS_SPACE(*last); ++last) {}
    in.erase(last.base(), in.end()); // [..)
    return in;
}

template <typename T>
inline
T trim(const T &in)
{
    // ltrim
    auto first(in.begin());
    for (; first != in.end() && RZ_STR_TRIM_IS_SPACE(*first); ++first) {}

    // rtrim
    auto last(in.rbegin());
    for (; last.base() != first && RZ_STR_TRIM_IS_SPACE(*last); ++last) {}

    return T(first, last.base());
}

template <typename T>
inline
T& trimInPlace(T &in)
{
    // rtrim
    auto last(in.rbegin());
    for (; last.base() != in.begin() && RZ_STR_TRIM_IS_SPACE(*last); ++last) {}
    in.erase(last.base(), in.end());

    // ltrim
    auto first(in.begin());
    for (; first != last.base() && RZ_STR_TRIM_IS_SPACE(*first); ++first) {}
    in.erase(in.begin(), first);

    return in;
}

template <typename T>
inline
T& trimInPlace_1(T &in)
{
    // ltrim
    auto first(in.begin());
    for (; first != in.end() && RZ_STR_TRIM_IS_SPACE(*first); ++first) {}

    // rtrim
    auto last(in.rbegin());
    for (; last.base() != first && RZ_STR_TRIM_IS_SPACE(*last); ++last) {}

    auto last2 = std::copy(first, last.base(), in.begin());
    in.erase(last2, in.end());
    return in;
}

template <typename T>
inline
T trim2(const T &in)
{
    // ltrim
    auto first = std::find_if(in.begin(), in.end(),
        [](const T::value_type &ch)->bool { return !RZ_STR_TRIM_IS_SPACE(ch); });

    // rtrim
    auto last = std::find_if(in.rbegin(), in.rend(),
        [](const T::value_type &ch)->bool { return !RZ_STR_TRIM_IS_SPACE(ch); });

    return T(first, last.base());
}

template <typename T>
inline
T& trimInPlace2(T &in)
{
    // rtrim
    auto last = std::find_if(in.rbegin(), in.rend(),
        [](const T::value_type &ch)->bool { return !RZ_STR_TRIM_IS_SPACE(ch); });
    in.erase(last.base(), in.end());

    // ltrim
    auto first = std::find_if(in.begin(), in.end(),
        [](const T::value_type &ch)->bool { return !RZ_STR_TRIM_IS_SPACE(ch); });
    in.erase(in.begin(), first);

    return in;
}

template <typename T>
inline
T&  trimInPlace2_1(T &in)
{
    // ltrim
    auto first = std::find_if(in.begin(), in.end(),
        [](const T::value_type &ch)->bool { return !RZ_STR_TRIM_IS_SPACE(ch); });

    // rtrim
    auto last = std::find_if(in.rbegin(), in.rend(),
        [](const T::value_type &ch)->bool { return !RZ_STR_TRIM_IS_SPACE(ch); });

    auto last2 = std::copy(first, last.base(), in.begin());
    in.erase(last2, in.end());
    return in;
}

template <typename InIt, typename OutIt>
inline
void trimTo(const InIt &in, const InIt &end, OutIt &out)
{
    // ltrim
    InIt first(in);
    for (; first != end && RZ_STR_TRIM_IS_SPACE(*first); ++first) {}

    // rtrim
    InIt last(end);
    for (; last != first && RZ_STR_TRIM_IS_SPACE(*(--last));) {}

    if (first != last)
        out = std::copy(first, last + 1, out);
}

template <typename In, typename Out>
inline
void trimTo(const In &in, Out &out)
{
    out.reserve(out.size() + in.size());
    auto outIt(std::back_inserter(out));
    trimTo(in.begin(), in.end(), outIt);
}

//
// normalizeLineEnd
//

// Convert diff line end pairs to <lineEnd>
// Return end pos in result
template <typename It>
It normalizeLineEnd(const It &in, const It &end)
{
    It it(in), ret(in);
    int endlTypePrev = 0;
    for (; it != end; ++it)
    {
        int endlType = ascii_endl(*it);
        if (endlType != 0) // Is line end
        {
            if ((endlTypePrev + endlType) == 0) // Found pair
            {
                endlTypePrev = 0;
                continue;
            }
            else
                *ret = static_cast<It::value_type>('\n');
        }
        else if (it != ret) // Has dx
            *ret = *it;
        endlTypePrev = endlType;
        ++ret;
    }
    return ret;
}

template <typename T>
__forceinline
void normalizeLineEnd(T &s)
{
    auto it = normalizeLineEnd(s.begin(), s.end());
    s.resize(it - s.begin());
}

template <typename OutIt, typename InIt>
void normalizeLineEndTo(const InIt &in, const InIt &end, OutIt &out)
{
    InIt it(in);
    int endlTypePrev = 0;
    for (; it != end; ++it)
    {
        int endlType = ascii_endl(*it);
        if (endlType != 0) // Is line end
        {
            if ((endlTypePrev + endlType) == 0) // Found pair
            {
                endlTypePrev = 0;
                continue;
            }
            else
                *out = '\n'; // static_cast<out::value_type::value_type>('\n');
        }
        else
            *out = *it;
        endlTypePrev = endlType;
        ++out;
    }
}

template <typename Out, typename In>
__forceinline
void normalizeLineEndTo(const In &s, Out &out)
{
    out.reserve(out.size() + s.size());
    auto it = std::back_inserter(out);
    normalizeLineEndTo(s.begin(), s.end(), it);
}

//
// Split
//

template <typename OutIt, typename It1, typename It2>
void split(OutIt &out,
    const It1 &in, const It1 &end,
    const It2 &delim, const It2 &delimEnd,
    bool keepEmpty)
{
    if (in >= end)
        return;
    It1 it(in);
    typedef OutIt::container_type::value_type str_t;
    if (delim == delimEnd) // Delimeter empty
    {
        ++out;
        *out = std::move(str_t(it, end));
        return;
    }
    while (true)
    {
        auto subEnd = std::search(it, end, delim, delimEnd);
        if (keepEmpty || !(it == subEnd))
        {
            ++out;
            *out = std::move(str_t(it, subEnd));
        }
        if (subEnd == end)
            break;
        it = subEnd + std::distance(delim, delimEnd);
    }
}

// delim as char[]
template <typename Out, typename In, size_t N>
inline
void split(Out &out, const In &s, const char(&delim)[N], bool keepEmpty = false)
{
    auto outIt(std::back_inserter(out));
    split(outIt,
        s.begin(), s.end(),
        &delim[0], &delim[N - 1],
        keepEmpty);
}

// delim as wchar_t[]
template <typename Out, typename In, size_t N>
inline
void split(Out &out, const In &s, const wchar_t(&delim)[N], bool keepEmpty = false)
{
    auto outIt(std::back_inserter(out));
    split(outIt,
        s.begin(), s.end(),
        &delim[0], &delim[N - 1],
        keepEmpty);
}

// delim as c-array or std::array
template <typename Out, typename In, typename D, size_t N>
inline
void split(Out &out, const In &s, const D(&delim)[N], bool keepEmpty = false)
{
    auto outIt(std::back_inserter(out));
    split(outIt,
        s.begin(), s.end(),
        &delim[0], &delim[N],
        keepEmpty);
}

//
//
//

template <typename It>
inline
bool utf8SkipBom(It &in, const It &end)
{
    It it(in);
    const int BOM_LEN = 3;
    const unsigned char BOM[BOM_LEN] = { 0xEF, 0xBB, 0xBF };
    int skipped = 0;
    for (; skipped < BOM_LEN && it != end &&
        static_cast<unsigned char>(*it) == BOM[skipped];
        ++skipped, ++it) {
    }
    bool ret = skipped == 3;
    if (ret)
        in = it;
    return ret;
}

template <typename T>
inline
void parsePair(const std::basic_string<T> &s, const T delim,
    std::basic_string<T> &key,
    std::basic_string<T> &value)
{
    const size_t pos = s.find_first_of(delim);
    if (pos != std::basic_string<T>::npos)
    {
        key = s.substr(0, pos);
        value = s.substr(pos + 1);
    }
    else
    {
        key = s;
        value.clear();
    }
}

template <typename T>
void parseSimpleIni(const std::basic_string<T> &s,
    std::map<std::basic_string<T>, std::basic_string<T>> &out)
{
    std::vector<std::basic_string<T>> lines;
    std::basic_string<T> copy;
    normalizeLineEndTo(s, copy);
    split(lines, copy, "\n");
    for (auto line : lines)
    {
        size_t pos = line.find_first_of(static_cast<T>(';'));
        if (std::basic_string<T>::npos != pos)
            line = line.substr(0, pos);
        std::basic_string<T> key;
        std::basic_string<T> value;
        parsePair(line, static_cast<T>('='), key, value);
        trimInPlace(key);
        trimInPlace(value);
        if (!key.empty())
            out[key] = value;
    }
}

//
// Path and FileNames
//

template<typename T, typename E>
void changeFileName(std::basic_string<T> &path, const E &fileName)
{
    // TODO Disk/Server delim
    typedef std::basic_string<T> S;
    if (!path.empty())
    {
        T delims[3] = { T('\\'), T('/') };
        auto pos = path.find_last_of(delims);
        if (S::npos == pos)
            path = fileName;
        else
            path.replace(pos + 1, S::npos, fileName);
    }
    else
        path = fileName;
}

} // namespace