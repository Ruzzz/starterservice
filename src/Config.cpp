// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#include <string>
#include <map>
#include <windows.h>

#include "file_utils.hpp"
#include "string_utils.hpp"
#include "Logger.hpp"
#include "Config.hpp"

bool Config::load(const std::wstring &path, Config &out)
{
    try
    {
        std::wstring content;
        if (!rz::fileReadAsUtf8(path, content))
        {
            std::wstring s(L"Cannot load config file: " + path + L".");
            Log::instance()->error(s.c_str());
            return false;
        }
        rz::trimInPlace(content);
        if (content.empty())
        {
            std::wstring s(L"Config file is empty: " + path + L".");
            Log::instance()->error(s.c_str());
        }

        rz::normalizeLineEnd(content);
        std::map<std::wstring, std::wstring> m;
        rz::parseSimpleIni(content, m);

        // interval

        if (m.find(L"interval") == m.end())
        {
            Log::instance()->error(
                L"Invalid config file. Required 'interval' not found.");
            return false;
        }
        try
        {
            out.interval_ = std::stoi(m[L"interval"]);
        }
        catch (...)
        {
            Log::instance()->error(
                L"Invalid config file. <interval> is not valid number.");
            return false;
        }
        if (out.interval_ < MIN_SECONDS || out.interval_ > MAX_SECONDS)
        {
            Log::instance()->error(
                L"Invalid config file. <interval> out of range.");
            return false;
        }
        out.interval_ *= 1000;

        // exe_path

        if (m.find(L"exe_path") == m.end())
        {
            Log::instance()->error(
                L"Invalid config file. Required <exe_path> not found.");
            return false;
        }
        out.exePath_ = m[L"exe_path"];
        rz::trimInPlace(out.exePath_);
        // Unquote
        if (!out.exePath_.empty() && out.exePath_.front() == L'"')
        {
            out.exePath_.pop_back();
            out.exePath_.erase(0, 1);
        }
        DWORD expLen = ::ExpandEnvironmentStringsW(out.exePath_.c_str(), NULL, 0);
        if (expLen)
        {
            std::wstring expBuf(expLen + 1, 0);
            ::ExpandEnvironmentStringsW(out.exePath_.c_str(), &expBuf[0],
                expLen + 1);
            out.exePath_.assign(&expBuf[0]);
        }
        if (out.exePath_.empty() || !rz::fileExists(out.exePath_.c_str()))
        {
            std::wstring s(L"Invalid config file. Path stored in <exe_path> not exists: "
                + m[L"exe_path"] + L".");
            Log::instance()->error(s.c_str());
            return false;
        }

        // session_id

        if (m.find(L"session_id") == m.end())
        {
            out.sessionID_ = 0;
        }
        else
        {
            try
            {
                out.sessionID_ = std::stoi(m[L"session_id"]);
            }
            catch (...)
            {
                Log::instance()->error(
                    L"Invalid config file. <session_id> is not valid number.");
                return false;
            }
        }

        // exe_params

        out.exeParams_ = out.exePath_;
        // Quote
        // if (out.preparedParams_.find_first_of(L" ") != std::wstring::npos)
        out.exeParams_ = L'"' + out.exeParams_ + L'"';
        
        if (m.find(L"exe_params") != m.end())
        {
            std::wstring params = m[L"exe_params"];
            rz::trimInPlace(params);
            if (!params.empty())
            {
                out.exeParams_.append(L" ");
                out.exeParams_.append(params);
            }
        }

    }
    catch (...)
    {
        Log::instance()->error(L"Configurios Ubitus.");
        return false;
    }

    return true;
}
