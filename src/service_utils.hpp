// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-03-15

#pragma once

#include <windows.h>

namespace rz {

// --- Return WinAPI Errors ---------------------------------------------------
BOOL serviceInstall(const wchar_t *name, const wchar_t *displayName,
    const wchar_t *path, DWORD startType, DWORD serviceType,
    const wchar_t *dependencies, const wchar_t *account, const wchar_t *password);
BOOL serviceUninstall(const wchar_t *name);
BOOL serviceStart(const wchar_t *name);
BOOL serviceStop(const wchar_t *name);

} // namespace