// Project: Starter service
// Author:  Ruslan Zaporojets
// Email:   ruzzzua[]gmail.com
// Date:    2016-12-31

#pragma once

#include <exception>
#include <windows.h>

//
//  Consts
//

extern const char *SERVICE_VERSION_A;
extern const char *EXE_NAME_A;
extern const wchar_t *CONFIG_FILENAME;
extern const char *CONFIG_FILENAME_A;

extern const wchar_t *SERVICE_NAME;
extern const wchar_t *SERVICE_DISPLAY_NAME;
extern const char *SERVICE_DISPLAY_NAME_A;
extern const wchar_t *SERVICE_DEPENDENCIES;

#define SERVICE_START_TYPE      SERVICE_AUTO_START
#define SERVICE_TYPE            SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS

// NT AUTHORITY\\SYSTEM
#define SERVICE_ACCOUNT         NULL
#define SERVICE_PASSWORD        NULL

#define SRV_BREAK ::OutputDebugStringA("Ruzzz ___ Break"); \
                  volatile int f = 1; \
                  while (f) \
                      ::Sleep(1000);

//
// Helpers
//

// ��� ������ VC++ �������� ���������
// ��������: ���������� � fn() �� ����� :)
template <typename F>
struct ScopeExit
{
    ScopeExit() = delete;
    ScopeExit(const F &fn) : fn_(fn) { }
    ~ScopeExit() { fn_(); }
private:
    F fn_;
};

#define RZ_EXIT_SCOPE2_(prefix, line) prefix ## line
#define RZ_EXIT_SCOPE_(prefix, line) RZ_EXIT_SCOPE2_(prefix, line)
#define RZ_EXIT_SCOPE(F) const auto RZ_EXIT_SCOPE_(rzExitScopeFn,__LINE__) = F; \
    const auto RZ_EXIT_SCOPE_(rzExitScopeVar,__LINE__) = \
    ScopeExit<decltype(RZ_EXIT_SCOPE_(rzExitScopeFn,__LINE__))>(RZ_EXIT_SCOPE_(rzExitScopeFn,__LINE__))

// ������ ��� ����������� �������, ������� �� ������ ���������� WinAPI
// � ��� �������� �� ���������� ���� message
class winapi_error : public std::exception
{
public:
    winapi_error() = delete;

    explicit winapi_error(const char *const winapiFunc, DWORD code)
        : std::exception(winapiFunc), code_(code)
    {
    }

    ~winapi_error() override
    {
    }

    DWORD code() const
    {
        return code_;
    }

private:
    DWORD code_;
};
